import React, {useState, useEffect, Fragment} from "react";
import axios from "axios";

import {Header} from "../header/header";
import {MessageList} from "../message-list/message-list";
import {InputMessage} from "../input-my-message/input-my-message";
import {Loading} from "../loading/loading";

export const Chat = () => {

    const [myMessages, setMessages] = useState([]);
    const [messagesList, setMessagesList] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(users => {
                setMessagesList(users.data)
                setLoading(false)
            })
    }, [])

    const deleteItem = id => {
        let idx = myMessages.findIndex( el => el.id === id);
        const newArray = [
            ...myMessages.slice(0, idx),
            ...myMessages.slice(idx + 1)
        ];

        setMessages(newArray);
    };

    const updateItem = (id, text) => {
        let idx = myMessages.findIndex( el => el.id === id);
        const newArray = [
            ...myMessages.slice(0, idx),
            {
                ...myMessages[idx],
                text
            },
            ...myMessages.slice(idx + 1)
        ];

        setMessages(newArray);
    }

    if(loading) {
        return <Loading />
    }

    const messages = loading
        ? <Loading />
        :(<Fragment>
            <MessageList
                setMessages={setMessages}
                myMessages = {myMessages}
                deleteItem = {deleteItem}
                updateItem = {updateItem}
                messagesList={messagesList}
            />
            <InputMessage setMessages={setMessages} myMessages = {myMessages} />
        </Fragment>)

    const lastMessageFunc = arr => {
     return arr[arr.length - 1].createdAt.slice(0,10) + ", " + arr[arr.length - 1].createdAt.slice(11, 19)
    }

    let lastMessage = 'xxxx-xx-xx'
    if (messagesList) {
        lastMessage = (myMessages.length !== 0) ? lastMessageFunc(myMessages) : lastMessageFunc(messagesList)
    }

    return <div className="chat">
        <Header
            countMessages={messagesList.length + myMessages.length}
            lastMessage={lastMessage}
        />
        {messages}
    </div>
}