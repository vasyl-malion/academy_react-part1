import React from "react";
import "./header.css";

export const Header = ({countMessages, lastMessage}) => {

    return <div className="header">
        <div>
            <span> My chat </span>
            <span> 4 participants </span>
            <span> {countMessages} messages </span>
        </div>
        <div>
            <span> last message at {lastMessage} </span>
        </div>
    </div>
}