import React, {useState} from "react";
import "./input-my-message.css";

export const InputMessage = ({setMessages, myMessages}) => {

    const [text, setText] = useState('');
    const date = `${new Date().toJSON().slice(0, 10)}, ${new Date().toJSON().slice(11, 19)}`;

    const addMessage = () => {

        if (!text) {
            return
        }

        const newMessage = {
            id: myMessages.length + 1,
            text,
            createdAt: date
        };

        const newArrMessages = [
            ...myMessages,
            newMessage
        ];
        setMessages(newArrMessages);
        setText("");
    }

    const keyFunc = e => {
        const enterKeyCode = 13;
        if (e.keyCode === enterKeyCode) {
            addMessage();
        }
    }

    return <div className="inputDiv">
        <input
            value={text}
            placeholder="type your message"
            onKeyUp={keyFunc}
            onChange={e => setText(e.target.value)}
        />
        <button onClick={addMessage}>Send</button>
    </div>
}