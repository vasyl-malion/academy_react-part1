import React, {useState, useEffect} from "react";

import {Message} from "../message/message";
import {MyMessage} from "../my-message/my-message";

import "./message-list.css";

export const MessageList = ({myMessages, deleteItem, messagesList, updateItem}) => {

    const myArrMessages = myMessages && myMessages.map( message => {
        return <MyMessage
            key = {message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
            deleteItem = {deleteItem}
            updateItem = {updateItem}
        />
    });

    const allMessages = messagesList && messagesList.map( message => {
        return <Message
            key = {message.id}
            avatar={message.avatar}
            text={message.text}
            user={message.user}
            createdAt={message.createdAt}
        />
    });

    return <div className="messagesList">
        {allMessages}
        {myArrMessages}
    </div>
}
