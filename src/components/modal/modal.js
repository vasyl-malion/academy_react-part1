import React from "react";
import "./modal.css";

export const Modal = ({id, text, updateItem, setEdit}) => {

    const [updatedText, setUpdatedText] = React.useState(text);

    const editMsg = () => {

        if (!updatedText) {
            return
        }

        updateItem(id, updatedText);
        setEdit(false);
    }

    return <div className="modal">
        <input
            value={updatedText}
            onChange={e => setUpdatedText(e.target.value)}
        />
        <button onClick={editMsg}> Edit </button>
    </div>
}