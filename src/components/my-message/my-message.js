import React from "react";
import {MdDelete} from "react-icons/md";
import {BsPencil} from "react-icons/bs";

import {Modal} from "../modal/modal";

import "./my-message.css";

export const MyMessage = ({id, text, createdAt, deleteItem, updateItem}) => {

    const [edit, setEdit] = React.useState(false);

    const modal = edit
        ? <Modal
            id = {id}
            text = {text}
            updateItem={updateItem}
            setEdit = {setEdit}
        />
        : null;

    return <div className="myMessageMain">
        <div key={id} className="myMessage">
            <div>
                <span className="my-message-text"> {text} </span>
                <div className="my-createdAt"> {createdAt} </div>
            </div>
        </div>
        <div className="my-message-actions">
            <div>
                <MdDelete onClick = {() => deleteItem(id)}/>
            </div>
            <div>
                <BsPencil onClick = {() => setEdit(true)}/>
            </div>
        </div>
        {modal}
    </div>
}