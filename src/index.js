import React from 'react';
import ReactDOM from 'react-dom';

import {Chat} from "./components/chat/chat";

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <Chat />
  </React.StrictMode>,
  document.getElementById('root')
);

